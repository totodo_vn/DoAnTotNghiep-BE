const mongoose = require("mongoose");
var ObjectID = require("mongodb").ObjectId;
const findOrCreate = require("mongoose-findorcreate");
const helper = require("../utils/helper");
const { userTypes } = require("../constants");
const Schema = mongoose.Schema;
const userSchema = new Schema({
    displayName: {
        type: String,
        required: true,
    },
    email: {
        type: String,
    },
    password: {
        type: String,
    },
    type: {
        type: Number,
        required: true,
        default: userTypes.NORMAL,
    },
    avatar: {
        type: String,
        required: true,
        default:
            "https://res.cloudinary.com/dwuma83gt/image/upload/v1612669308/tasks-management/istockphoto-898400566-612x612_ctxvke.jpg",
    },
    idAccountOwner: {
        type: String,
    },
    isActivated: {
        type: Boolean,
        default: false,
    },
    habit: [{ type: ObjectID }],
});

userSchema.plugin(findOrCreate);

const User = mongoose.model("User", userSchema, "users");
module.exports = {
    User,
    addUser: (user) => {
        const newUser = new User(user);
        newUser.password = helper.generateHashPassword(newUser.password);
        return User.create(newUser);
    },
    activateUserByEmail: async (email) => {
        return await User.findOneAndUpdate(
            { email: email },
            { isActivated: true }
        )
            .then((result) => result)
            .catch((error) => {
                throw new Error(error);
            });
    },
    getAllUsers: async () => {
        return await User.find({})
            .then((result) => result)
            .catch((error) => {
                throw new Error(error);
            });
    },
    getSingleUserById: async (userId) => {
        console.log("ID: ", userId);
        return await User.findById(userId)
            .then((result) => result)
            .catch((error) => {
                throw new Error(error);
            });
    },
    getSingleUserByEmail: async (email) => {
        return await User.findOne({ email: email })
            .then((result) => result)
            .catch((error) => {
                throw new Error(error);
            });
    },
    // hide field password
    getUserWithoutPasswordField: (user) => {
        const newUser = { ...user }._doc;
        delete newUser.password;
        return newUser;
    },
};
