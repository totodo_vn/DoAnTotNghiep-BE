const mongoose = require("mongoose");
var ObjectID = require("mongodb").ObjectId;
const helper = require("../utils/helper");
const Schema = mongoose.Schema;
const LabelSchema = new Schema({
    name: {
        type: String,
        required: true,
        default: "Label #1"
    },
    color: {
        type: String,
        required: true,
        default: "red",
    },
    isFavorite: {
        type: Boolean,
        required: true,
        default: false,
    }
},
{
    timestamps: true,
});

const Label = mongoose.model("Label", LabelSchema, "labels");
module.exports = {
    Label,
};
