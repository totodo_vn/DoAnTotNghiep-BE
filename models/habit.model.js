const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const HabitSchema = new Schema(
    {
        name: {
            type: String,
            required: true,
            default: "Habit #default",
        },
        icon: {
            iconColor: String,
            iconText: String,
            iconImage: String,
        },
        images: {
            imgBg: String,
            imgUnCheckIn: String,
            imgCheckIn: String,
        },
        isSaveDiary: Boolean,
        remind: {
            hour: Number,
            minute: Number,
        },
        motivation: {
            content: String,
            images: [],
        },
        habitTotalDay: Number,
        habitFinishDate: Date,
        missionDayUnit: String,
        missionDayCheckInStep: Number,
        totalTimesMustCheckIn: Number,
        isFinished: Boolean,
        frequency: {
            typeFrequency: String,
            dailyDays: [],
            weeklyDays: Number,
            weeklyUnit: String,
            intervalDays: Number,
        },
        habitProgress: [
            {
                diaries: [{ content: String, images: [] }],
                currentCheckInTimes: {
                    type: Number,
                    default: 0,
                },
                isDone: { type: Boolean, default: false },
                date: Date,
            },
        ],
    },
    {
        timestamps: true,
    }
);

const HabitModel = mongoose.model("Habit", HabitSchema, "habits");

const addHabit = (habit) => {
    return HabitModel.create(habit);
};

const getHabits = () => {
    return HabitModel.find();
};

const getById = (id) => {
    return HabitModel.findById(id);
};

const deletedById = (id) => {
    return HabitModel.deleteOne({ _id: id });
};

const habitModelControl = {
    addHabit,
    getHabits,
    getById,
    deletedById,
};

module.exports = {
    HabitModel,
    habitModelControl,
};
