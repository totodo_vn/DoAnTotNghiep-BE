const mongoose = require("mongoose");
var ObjectID = require("mongodb").ObjectId;
const { viewTypes } = require("../constants");
const helper = require("../utils/helper");
const Schema = mongoose.Schema;
const ProjectSchema = new Schema({
    name: {
        type: String,
        required: true,
        default: "Project #1"
    },
    description: {
        type: String,
        required: true,
        default: "Todolist"
    },
    color: {
        type: String,
        required: true,
        default: "red",
    },
    isFavorite: {
        type: Boolean,
        default: false,
    },
    viewType: {
        type: Number,
        default: viewTypes.LIST,
    },
    authorId: {
        type: ObjectID,
        required: true
    },
    sections: {
        type: Array,
        default: []
    }
},
{
    timestamps: true,
});

const Project = mongoose.model("Project", ProjectSchema, "projects");
module.exports = {
    Project,
};
