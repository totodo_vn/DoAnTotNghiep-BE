const mongoose = require("mongoose");
var ObjectID = require("mongodb").ObjectId;
const helper = require("../utils/helper");
const Schema = mongoose.Schema;
const TaskSchema = new Schema({
    projectId: {
        type: String,
        required: true,
    },
    sectionId: {
        type: String,
        required: true
    },
    priority: {
        type: Number,
        required: true,
        default: 1
    },
    name: {
        type: String,
        required: true,
        default: "New Task",
    },
    labelIds: {
        type: Array,
        default: [],
    },
    description: {
        type: String,
    },
    point: {
        type: Number,
        default: 0,
    },
    isMember: {
        type: Boolean,
        required: true,
        default: false,
    },
    isStarred: {
        type: Boolean,
        required: true,
        default: false,
    },
    isImportant: {
        type: Boolean,
        required: true,
        default: false,
    },
    isTrashed: {
        type: Boolean,
        required: true,
        default: false,
    },
    isCompleted: {
        type: Boolean,
        required: true,
        default: false,
    },
    completedDate: {
        type: String,
    },
    dueDate: {
        type: String,
    },
    crontabSchedule: {
        type: String,
    },
    preciseSchedules: {
        type: Array,
        default: [],
    },
    checkList: {
        type: Array,
        default: [],
    },
    attachmentInfos: {
        type: Array,
        default: [],
    },
},
{
    timestamps: true,
});

const Task = mongoose.model("Task", TaskSchema, "tasks");
module.exports = {
    Task,
};
