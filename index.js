const express = require("express");
const swaggerUi = require("swagger-ui-express");
const swaggerJsDoc = require("swagger-jsdoc");
var cors = require("cors");
const connectDB = require("./utils/database");
const logger = require("morgan");
const passport = require("passport");

const app = express();

require("dotenv").config({ silent: process.env.NODE_ENV === "production" });

const passportLocal = require("./config/passport");

connectDB();
app.use(cors());
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(logger("dev"));
app.use(passport.initialize());

app.get("/", (req, res) => {
    return res.end("Do An Tot Nghiep v1.0 dev");
});
app.use("/users", require("./routers/users.router"));
app.use("/labels", require("./routers/labels.router"));
app.use("/projects", require("./routers/projects.router"));
app.use("/tasks", require("./routers/tasks.router"));
app.use("/habits", require("./routers/habits.route"));

//#region swagger
// for api document
const swaggerOptions = {
    swaggerDefinition: {
        openapi: "3.0.0",
        info: {
            title: "Personal Tasks Management APIs",
            version: "1.0.0",
        },
        servers: [{ url: `http://${process.env.SERVER_HOST}:3006` }],
    },
    apis: ["./routers/*.js"],
};
const swaggerDocs = swaggerJsDoc(swaggerOptions);
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocs));
//#endregion

app.listen(process.env.PORT || 3001, () =>
    console.log(
        `Server is running at http://localhost:${process.env.PORT || 3006}`
    )
);
