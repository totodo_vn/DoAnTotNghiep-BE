const express = require("express");
const router = express.Router();

const tasksController = require("../controllers/tasks.controller");
const auth = require("../middlewares/auth");

router.post("/", auth, tasksController.createNewTask);
router.get("/", auth, tasksController.getAllTasks);
router.get("/:taskId", auth, tasksController.getTaskDetail);
router.put("/:taskId", auth, tasksController.updateTaskDetail);
// router.delete("/:taskId", auth, tasksController.deleteNewTask);

/**
 * @swagger
 * components:
 *   schemas:
 *     Task:
 *       type: object
 *       properties:
 *          taskId:
 *             type: string
 *             description: auto-generated
 *          projectId:
 *             type: id
 *             description: Id dự án
 *          sectionId:
 *             type: id
 *             description: Id đề mục (tự chọn)
 *          priority:
 *             type: string
 *             description: Mức độ ưu tiên
 *          name:
 *             type: string
 *             description: Tên công việc
 *          description:
 *             type: string
 *             description: Mô tả công việc
 *          point:
 *             type: Number
 *             description: Mức điểm cho công việc này
 *          isMember:
 *             type: Boolean
 *          isStarred:
 *             type: Boolean
 *          isImportant:
 *             type: Boolean
 *          isTrashed:
 *             type: Boolean
 *          isCompleted:
 *             type: Boolean
 *          completedDate:
 *             type: String
 *          dueDate:
 *             type: String
 *          crontabSchedule:
 *             type: String
 *          preciseSchedules:
 *             type: String
 *          labelIds:
 *             type: Array
 *             description: id các label liên quan
 *          checkList:
 *             type: Array
 *             description: Tên các label liên quan và mã màu
 *          attachmentInfos:
 *             type: Array
 *             description: Tên các label liên quan và mã màu
 *       example:
 *          projectId: 101239
 *          sectionId: 1231283
 *          labelIds: []
 *          priority: normal
 *          name: Tạo server mới
 *          description: Server cho front end app todolist
 *          point: 0
 *          isMember: false
 *          isStarred: false 
 *          isImportant: false
 *          isTrashed: false
 *          isCompleted: false
 *          completedDate: null
 *          dueDate: false
 *          crontabSchedule: false
 *          preciseSchedules: false
 *          checkList: []
 *          attachmentInfos: []
 *   responses:
 *     UnauthorizedError:
 *       description: Access token is missing or invalid
 *   securitySchemes:
 *     bearerAuth:
 *       type: http
 *       scheme: bearer
 *       bearerFormat: JWT
 */

/**
 * @swagger
 * tags:
 *   name: Tasks
 *   description: API for Tasks
 */

/**

/**
 * @swagger
 * /tasks:
 *   post:
 *     summary: Tạo một task mới
 *     tags: [Tasks]
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       content:
 *         application/json:
 *           schema:
 *               $ref: '#/components/schemas/Task'
 *               properties:
 *                  displayName:
 *                      type: string
 *     responses:
 *       '200':
 *         description: Tạo thành công
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 succeeded:
 *                      type: boolean
 *                 message: 
 *                      type: string
 *                 result:
 *                      type: object
 *                      properties:
 *                          user:
 *                              type: object
 *                          accessToken: 
 *                              type: string
 *                          refreshToken:
 *                              type: string
 *       '400':
 *         description: Không đủ body hoặc không có email trong hệ thống
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 succeeded:
 *                   type: boolean
 *                 message:
 *                   type: string
 *                 result:
 *                   type: object
 *   get:
 *     summary: Lấy mọi task trong hệ thống
 *     tags: [Tasks]
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       '200':
 *         description: Lấy thành công
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 succeeded:
 *                   type: boolean
 *                 message:
 *                   type: string
 *                 result:
 *                   type: object
 *                   properties:
 *                     user:
 *                       type: object
 *       '400':
 *         description: Không đủ trong body
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 succeeded:
 *                   type: boolean
 *                 message:
 *                   type: string
 *                 result:
 *                   type: object
 * /tasks/{taskId}:
 *   get:
 *     summary: Lấy thông tin 1 task
 *     tags: [Tasks]
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: taskId
 *         required: false
 *         schema:
 *             type: String
 *     responses:
 *       '200':
 *         description: Thàng công
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 succeeded:
 *                   type: boolean
 *                 message:
 *                   type: string
 *                 result:
 *                   type: object
 *                   properties:
 *                     user:
 *                       type: object
 *       '400':
 *         description: Không đủ trong body, không có project
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 succeeded:
 *                   type: boolean
 *                 message:
 *                   type: string
 *                 result:
 *                   type: object
 */

module.exports = router;
