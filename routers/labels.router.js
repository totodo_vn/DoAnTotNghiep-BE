const express = require("express");
const router = express.Router();

const labelsController = require("../controllers/labels.controller");
const auth = require("../middlewares/auth");

router.post("/", auth, labelsController.createNewLabel);
router.get("/", auth, labelsController.getAllLabels);
router.get("/:labelId", auth, labelsController.getLabelDetail);
router.put("/:labelId", auth, labelsController.updateLabelDetail);
router.put("/toggle-favorite/:labelId", auth, labelsController.toggleIsFavoriteLabel);

/**
 * @swagger
 * components:
 *   schemas:
 *     Label:
 *       type: object
 *       properties:
 *          name:
 *             type: string
 *             description: Tên label
 *          color:
 *             type: string
 *             description: Mã màu của label theo tên màu
 *          isFavorite:
 *             type: boolean
 *       example:
 *          name: Backend
 *          color: Backend
 *          isFavorite: false
 *   responses:
 *     UnauthorizedError:
 *       description: Access token is missing or invalid
 *   securitySchemes:
 *     bearerAuth:
 *       type: http
 *       scheme: bearer
 *       bearerFormat: JWT
 */

/**
 * @swagger
 * tags:
 *   name: Labels
 *   description: API for Label
 */

/**

/**
 * @swagger
 * /labels:
 *   post:
 *     summary: Tạo một label mới
 *     tags: [Labels]
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       content:
 *         application/json:
 *           schema:
 *               $ref: '#/components/schemas/Label'
 *     responses:
 *       '200':
 *         description: Tạo thành công
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 succeeded:
 *                      type: boolean
 *                 message: 
 *                      type: string
 *                 result:
 *                      type: object
 *                      properties:
 *                          schema:
 *                              $ref: '#/components/schemas/Label'
 *       '400':
 *         description: Trùng thông tin name/color trong hệ thống
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 succeeded:
 *                   type: boolean
 *                 message:
 *                   type: string
 *                 result:
 *                   type: object
 *       '500':
 *         description: Hệ thống lỗi hoặc thiếu thông tin body request
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 succeeded:
 *                   type: boolean
 *                 message:
 *                   type: string
 *                 result:
 *                   type: object
 *   get:
 *     summary: Lấy tất cả labels trong hệ thống
 *     tags: [Labels]
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       '200':
 *         description: Lấy dữ liệu thành công
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 succeeded:
 *                   type: boolean
 *                 message:
 *                   type: string
 *                 result:
 *                   type: object
 *       '500':
 *         description: Hệ thống không thể lấy được
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 succeeded:
 *                   type: boolean
 *                 message:
 *                   type: string
 *                 result:
 *                   type: object
 * /labels/{labelId}:
 *   get:
 *     summary: Lấy thông tin 1 label
 *     tags: [Labels]
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: labelId
 *         required: false
 *         schema:
 *             type: String
 *     responses:
 *       '200':
 *         description: Lấy dữ liệu thành công
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 succeeded:
 *                   type: boolean
 *                 message:
 *                   type: string
 *                 result:
 *                   type: object
 *                   properties:
 *                     user:
 *                       type: object
 *       '400':
 *         description: Không tìm được label hoặc hệ thống bị lỗi
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 succeeded:
 *                   type: boolean
 *                 message:
 *                   type: string
 *                 result:
 *                   type: object
 * 
 *   put:
 *     summary: Cập nhật thông tin 1 label
 *     tags: [Labels]
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: labelId
 *         required: false
 *         schema:
 *             type: String
 *     requestBody:
 *       content:
 *         application/json:
 *           schema:
 *               $ref: '#/components/schemas/Label'
 *     responses:
 *       '200':
 *         description: Thay đổi thông tin thành công
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 succeeded:
 *                   type: boolean
 *                 message:
 *                   type: string
 *                 result:
 *                   type: object
 *                   properties:
 *                     user:
 *                       type: object
 *       '400':
 *         description: Không tìm được label hoặc hệ thống bị lỗi
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 succeeded:
 *                   type: boolean
 *                 message:
 *                   type: string
 *                 result:
 *                   type: object
 * /labels/toggle-favorite/{labelId}:
 *   put:
 *     summary: Cập nhật tình trạng yêu thích của 1 label, thích rồi gọi lại sẽ không thích nữa.
 *     tags: [Labels]
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: labelId
 *         required: false
 *         schema:
 *             type: String
 *     requestBody:
 *       content:
 *         application/json:
 *           schema:
 *               $ref: '#/components/schemas/Label'
 *     responses:
 *       '200':
 *         description: Cập nhật thành công
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 succeeded:
 *                   type: boolean
 *                 message:
 *                   type: string
 *                 result:
 *                   type: object
 *                   properties:
 *                     user:
 *                       type: object
 *       '400':
 *         description: Không tìm được label hoặc hệ thống bị lỗi
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 succeeded:
 *                   type: boolean
 *                 message:
 *                   type: string
 *                 result:
 *                   type: object
 */

module.exports = router;
