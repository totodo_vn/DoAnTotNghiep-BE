const express = require("express");
const router = express.Router();

const projectsController = require("../controllers/projects.controller");
const auth = require("../middlewares/auth");

router.post("/", auth, projectsController.createNewProject);
router.get("/", auth, projectsController.getAllProjects);
router.get("/:projectId", auth, projectsController.getProjectDetail);
router.post("/:projectId/sections/", auth, projectsController.createNewSectionInProject);
// router.get("/:projectId/sections/:sectionId", auth, projectsController.createNewSectionIn);
// router.put("/:projectId/sections/:sectionId", auth, projectsController.createNewSectionIn);

// router.put("/:taskId", auth, projectsController.updateTaskDetail);
// router.delete("/:taskId", auth, projectsController.deleteNewTask);

/**
 * @swagger
 * components:
 *   schemas:
 *     Project:
 *       type: object
 *       properties:
 *          name:
 *             type: string
 *             description: auto-generated
 *          description:
 *             type: id
 *             description: mô tả dự án
 *          authorId:
 *             type: id
 *             description: id tác giả
 *          sections:
 *             type: Array
 *             description: mảng section có trong project
 *       example:
 *          name: Todoist
 *          description: Dự án cá nhân
 *          authorId: 6024c6389403f977fcfa7f55
 *          sections: [{ id: 1, name: "Inbox" }]
 *     Section:
 *       type: object
 *       properties:
 *          id:
 *             type: string
 *             description: auto-generated
 *          name:
 *             type: string
 *             description: auto-generated
 *          description:
 *             type: id
 *             description: mô tả dự án
 *       example:
 *          name: Doing
 *          description: Những việc đang làm
 *   responses:
 *     UnauthorizedError:
 *       description: Access token is missing or invalid
 *   securitySchemes:
 *     bearerAuth:
 *       type: http
 *       scheme: bearer
 *       bearerFormat: JWT
 */

/**
 * @swagger
 * tags:
 *   name: Projects
 *   description: API for Project
 */

/**

/**
 * @swagger
 * /projects:
 *   post:
 *     summary: Tạo một project mới
 *     tags: [Projects]
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       content:
 *         application/json:
 *           schema:
 *               $ref: '#/components/schemas/Project'
 *     responses:
 *       '200':
 *         description: Tạo thành công
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 succeeded:
 *                      type: boolean
 *                 message: 
 *                      type: string
 *                 result:
 *                      type: object
 *       '400':
 *         description: Bị trùng tên của Project
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 succeeded:
 *                   type: boolean
 *                 message:
 *                   type: string
 *                 result:
 *                   type: object
 *       '500':
 *         description: Không thể tạo được, lỗi hệ thống
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 succeeded:
 *                   type: boolean
 *                 message:
 *                   type: string
 *                 result:
 *                   type: object
 * 
 *   get:
 *     summary: Lấy thông tin tất cả project
 *     tags: [Projects]
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       '200':
 *         description: Lấy dữ liệu thành công
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 succeeded:
 *                   type: boolean
 *                 message:
 *                   type: string
 *                 result:
 *                   type: object
 *       '400':
 *         description: Không tìm được trong hệ thống.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 succeeded:
 *                   type: boolean
 *                 message:
 *                   type: string
 *                 result:
 *                   type: object
 * /projects/{projectId}:
 *   get:
 *     summary: Lấy thông tin 1 project
 *     tags: [Projects]
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: projectId
 *         required: false
 *         schema:
 *             type: String
 *     responses:
 *       '200':
 *         description: Lấy dữ liệu thành công
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 succeeded:
 *                   type: boolean
 *                 message:
 *                   type: string
 *                 result:
 *                   type: object
 *       '400':
 *         description: Không tìm được trong hệ thống.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 succeeded:
 *                   type: boolean
 *                 message:
 *                   type: string
 *                 result:
 *                   type: object
 * /projects/{projectId}/sections:
 *   post:
 *     summary: Tạo một section mới trong Project đã có
 *     tags: [Projects]
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: projectId
 *         required: false
 *         schema:
 *             type: String
 *     requestBody:
 *       content:
 *         application/json:
 *           schema:
 *               $ref: '#/components/schemas/Section'
 *     responses:
 *       '200':
 *         description: Tạo thành công
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 succeeded:
 *                   type: boolean
 *                 message:
 *                   type: string
 *                 result:
 *                   type: object
 *       '400':
 *         description: Không tìm được trong hệ thống.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 succeeded:
 *                   type: boolean
 *                 message:
 *                   type: string
 *                 result:
 *                   type: object
 */

module.exports = router;
