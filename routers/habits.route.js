const express = require("express");
const router = express.Router();

const auth = require("../middlewares/auth");
const habitsController = require("../controllers/habits.controller");

// nho them auth vao

router.post("/", habitsController.createHabit);
router.get("/", habitsController.getAll);
router.get("/:id", habitsController.getById);
router.put("/:id/check-in", habitsController.checkIn);
router.put("/:id/finish", habitsController.finishHabit);
router.patch("/:id", auth, (req, res) => {
    // update a habit by id
});
router.delete("/:id", habitsController.deleteHabit);

module.exports = router;

/**
 * @swagger
 * components:
 *   schemas:
 *     Habit:
 *       type: object
 *       properties:
 *          id:
 *             type: string
 *             description: auto-generated
 *          name:
 *             type: string
 *             description: Tên habit
 *          icon:
 *             type: object
 *             description: Dùng làm avatar cho habit
 *             properties:
 *                 iconColor:
 *                     type: string
 *                 iconText:
 *                     type: string
 *                 iconImage:
 *                      type: string
 *          images:
 *             type: object
 *             description: Các ảnh cho habit
 *             properties:
 *                 imgBg:
 *                     type: string
 *                 imgUnCheckIn:
 *                     type: string
 *                 imgCheckIn:
 *                      type: string
 *          isSaveDiary:
 *              type: boolean
 *              description: Check có ghi lại nhật ký không
 *          remind:
 *              type: object
 *              properties:
 *                      hour:
 *                           type: number
 *                      minute:
 *                          type: number
 *              description: Thời gian nhắc nhở mỗi ngày
 *          motivation:
 *              type: object
 *              properties:
 *                      text:
 *                           type: string
 *                      images:
 *                          type: array
 *          habitTotalDay:
 *              type: number
 *              description: Tổng số ngày để hoàn thành habit
 *          habitFinishDate:
 *              type: date
 *              description: Ngày hoàn thành
 *          missionDayUnit:
 *              type: number
 *              description: Đơn vị cho mỗi lần check in habit mỗi ngày
 *          missionDayCheckInStep:
 *              type: number
 *              description: Định nghĩa số lượng hoàn thành cho mỗi lần check in
 *          totalTimesMustCheckIn:
 *              type: number
 *              description: Số lần phải check in để hoàn thành 1 ngày thói quen
 *          isFinished:
 *              type: boolean
 *              description: Đã kết thúc chưa
 *          frequency:
 *              type: object
 *              description: Tần suất thực hiện thói quen
 *              properties:
 *                  typeFrequency:
 *                      type: string
 *                      description: 3 loại daily, weekly và interval
 *                  dailyDays:
 *                      type: array
 *                      description: Danh sách các thứ mấy trong tuần sẽ thực hiện
 *                  weeklyDays:
 *                      type: number
 *                      description: Số ngày thực hiện trên 1 unit
 *                  weeklyUnit:
 *                      type: string
 *                      description: Đơn vị, tuần, tháng hoặc năm
 *                  intervalDays:
 *                      type: number
 *                      description: Sau mỗi xxx ngày sẽ thực hiện
 *          habitProgress:
 *              type: array
 *              description: Mảng lưu quá trình thực hiện thói quen hằng ngày
 *              items:
 *                  type: object
 *                  properties:
 *                       diaries:
 *                           type: array
 *                           description: Danh sách các nhật ký cho 1 ngày thực hiện thói quen
 *                           items:
 *                              type: object
 *                              properties:
 *                                  text:
 *                                      type: string
 *                                  images:
 *                                      type: array
 *                       currentCheckInTimes:
 *                           type: number
 *                           description: Số lần check in hiện tại
 *                       isDone:
 *                          type: boolean
 *                          description: Đã hoàn thành chưa?
 *                       day:
 *                          type: date
 *                          description: Ngày thực hiện
 *       example:
 *          id: abc
 *
 */
