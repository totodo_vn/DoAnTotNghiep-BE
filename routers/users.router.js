const express = require("express");
const router = express.Router();
const passport = require("passport");

const usersController = require("../controllers/users.controller");
const auth = require("../middlewares/auth");
const User = require("../models/user.model");

router.post("/change-password", auth, usersController.changePassword);
router.post("/check-exist-email", usersController.checkExistEmail);
router.post("/reset-password/request", usersController.requestResetPassword);
router.post("/reset-password/verify", usersController.verifyOtpResetPassword);
router.post("/reset-password", usersController.resetPassword);
router.post("/login", usersController.login);
router.post("/register", usersController.register);
router.post("/activation/request", usersController.requestActivateAccount);
router.get("/activation/:activationToken", usersController.activateAccount);
router.post("/save-account-oauth", usersController.saveAccountOAuth);
router.get(
    "/verify-facebook-login",
    passport.authenticate("facebook-token"),
    usersController.verifyFacebookLogin
);
router.get(
    "/verify-google-login",
    passport.authenticate("google-token"),
    usersController.verifyGoogleLogin
);

/**
 * @swagger
 * components:
 *   schemas:
 *     User:
 *       type: object
 *       properties:
 *          id:
 *             type: string
 *             description: auto-generated
 *          displayName:
 *             type: string
 *             description: Tên đầy đủ của người dùng
 *          email:
 *             type: string
 *             description: Email là duy nhất
 *          password:
 *             type: string
 *             description: Được hashing
 *       example:
 *          id: abc
 *   responses:
 *     UnauthorizedError:
 *       description: Access token is missing or invalid
 *   securitySchemes:
 *     bearerAuth:
 *       type: http
 *       scheme: bearer
 *       bearerFormat: JWT
 */

/**
 * @swagger
 * tags:
 *   name: Users
 *   description: API for Users
 */

/**

/**
 * @swagger
 * /users/login:
 *   post:
 *     summary: User login
 *     tags: [Users]
 *     requestBody:
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               email:
 *                 type: string
 *                 value: khactrieu98@gmail.com
 *               password:
 *                 type: string
 *     responses:
 *       '200':
 *         description: Đăng ký thành công
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 succeeded:
 *                      type: boolean
 *                 message: 
 *                      type: string
 *                 result:
 *                      type: object
 *                      properties:
 *                          user:
 *                              type: object
 *                          accessToken: 
 *                              type: string
 *                          refreshToken:
 *                              type: string
 *       '400':
 *         description: Không đủ body hoặc không có email trong hệ thống
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 succeeded:
 *                   type: boolean
 *                 message:
 *                   type: string
 *                 result:
 *                   type: object
 * /users/register:
 *   post:
 *     summary: User register
 *     tags: [Users]
 *     requestBody:
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               displayName:
 *                 type: string
 *               email:
 *                 type: string
 *               password:
 *                 type: string
 *     responses:
 *       '200':
 *         description: Đăng ký thành công
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 succeeded:
 *                   type: boolean
 *                 message:
 *                   type: string
 *                 result:
 *                   type: object
 *                   properties:
 *                     user:
 *                       type: object
 *       '400':
 *         description: Không đủ trong body, email đã có người sử dụng, không đăng ký được
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 succeeded:
 *                   type: boolean
 *                 message:
 *                   type: string
 *                 result:
 *                   type: object
 *       
 * /users/change-password:
 *   post:
 *     summary: User change password
 *     tags: [Users]
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               old_password:
 *                 type: string
 *               password:
 *                 type: string
 *     responses:
 *       '200':
 *         description: Đổi password thành công
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 succeeded:
 *                   type: boolean
 *                 message:
 *                   type: string
 *                 result:
 *                   type: object
 *       '400':
 *         description: Đổi password thất bại
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 succeeded:
 *                   type: boolean
 *                 message:
 *                   type: string
 *                 result:
 *                   type: object
 * /users/check-exist-email:
 *   post:
 *     summary: Check email có tồn tại không (giúp Quên mật khẩu)
 *     tags: [Users]
 *     requestBody:
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               email:
 *                 type: string
 *     responses:
 *       '200':
 *         description: Kết quả việc kiểm tra mail có trong hệ thống không?
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 succeeded:
 *                   type: boolean
 *                 message:
 *                   type: string
 *                 result:
 *                   type: object
 * /users/reset-password:
 *   post:
 *     summary: Đổi mật khẩu mới khi quên mật khẩu (đã lấy otpcode từ /reset-password/request)
 *     tags: [Users]
 *     requestBody:
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               email:
 *                 type: string
 *               otpCode:
 *                 type: string
 *               newPassword:
 *                 type: string
 *     responses:
 *       '200':
 *         description: Reset mật khẩu thành công
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 succeeded:
 *                   type: boolean
 *                 message:
 *                   type: string
 *                 result:
 *                   type: object
 *       '400':
 *         description: Request body không đủ hoặc OTP không hợp lệ
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 succeeded:
 *                   type: boolean
 *                 message:
 *                   type: string
 *                 result:
 *                   type: object
 * /users/reset-password/request:
 *   post:
 *     summary: Lấy OTP code qua email để set mật khẩu mới
 *     tags: [Users]
 *     requestBody:
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               email:
 *                 type: string
 *     responses:
 *       '200':
 *         description: Gửi OTP code qua email thành công
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 succeeded:
 *                   type: boolean
 *                 message:
 *                   type: string
 *                 result:
 *                   type: object
 *       '400':
 *         description: Request body không đủ hoặc email không hợp lệ
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 succeeded:
 *                   type: boolean
 *                 message:
 *                   type: string
 *                 result:
 *                   type: object
 *       '500':
 *         description: Không thể gửi OTP code qua email
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 succeeded:
 *                   type: boolean
 *                 message:
 *                   type: string
 *                 result:
 *                   type: object 
 * /users/reset-password/verify:
 *   post:
 *     summary: Kiểm tra tính hợp lệ của OTP với email người dùng
 *     tags: [Users]
 *     requestBody:
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               email:
 *                 type: string
 *               otpCode:
 *                 type: string
 *     responses:
 *       '200':
 *         description: OTP hợp lệ
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 succeeded:
 *                   type: boolean
 *                 message:
 *                   type: string
 *                 result:
 *                   type: object
 *       '400':
 *         description: Request body không đủ hoặc email hay OTP không hợp lệ
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 succeeded:
 *                   type: boolean
 *                 message:
 *                   type: string
 *                 result:
 *                   type: object
 * 
 * /users/activation/request:
 *   post:
 *     summary: Gửi yêu cầu đưa link kích hoạt tài khoản nếu chưa kích hoạt được.
 *     tags: [Users]
 *     requestBody:
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               email:
 *                 type: string
 *     responses:
 *       '200':
 *         description: Tạo link gửi email được hoặc không cần phải gửi vì đã kích hoạt rồi
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 succeeded:
 *                   type: boolean
 *                 message:
 *                   type: string
 *                 result:
 *                   type: object
 *       '400':
 *         description: Email không hợp lệ hoặc ko có tài khoản.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 succeeded:
 *                   type: boolean
 *                 message:
 *                   type: string
 *                 result:
 *                   type: object
 *       '500':
 *         description: Server không thể gửi email được.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 succeeded:
 *                   type: boolean
 *                 message:
 *                   type: string
 *                 result:
 *                   type: object 
 * /users/activation/:
 *   get:
 *     summary: Link để kích hoạt tài khoản (được gửi trong email).
 *     tags: [Users]
 *     responses:
 *       '200':
 *         description: Kích hoạt tài khoản thành công hoặc không cần kích hoạt
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 succeeded:
 *                   type: boolean
 *                 message:
 *                   type: string
 *                 result:
 *                   type: object
 *       '400':
 *         description: Email không hợp lệ hoặc ko có tài khoản.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 succeeded:
 *                   type: boolean
 *                 message:
 *                   type: string
 *                 result:
 *                   type: object
 *       
 * /users/save-account-oauth:
 *   post:
 *     summary: Login với OAuth2
 *     tags: [Users]
 *     requestBody:
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               idAccountOwner:
 *                 type: string
 *               type:
 *                 type: number
 *                 default: 1 
 *                 description: 1 - FB hoac 2 - Google 
 *               displayName:
 *                 type: string
 *               avatar:
 *                 type: string
 * 
 *     responses:
 *       '200':
 *         description: Lưu lại thành công
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 succeeded:
 *                   type: boolean
 *                 message:
 *                   type: string
 *                 result:
 *                   type: object
 *       
 */

module.exports = router;
