const bcryptjs = require("bcryptjs");
const randomstring = require("randomstring");
const { totp } = require("otplib");
const User = require("../models/user.model");
const { User: UserModel } = require("../models/user.model");
const bcrypt = require("bcryptjs");
const nodemailer = require("nodemailer");
const helper = require("../utils/helper");
const { Project } = require("../models/project.model");
const responseHelper = require("../utils/response");

totp.options = { step: 300 };

const login = async (req, res) => {
    const { email, password } = req.body;

    // Check email vs password is not null
    if (!password || helper.checkEmailIsValid(email) == false) {
        return responseHelper.badRequestResponse(
            res,
            false,
            "Email và password bị thiếu hoặc không hợp lệ!",
            null
        );
    }

    // Get a user by email
    const result = await User.getSingleUserByEmail(email);
    if (!result || result.length === 0) {
        return responseHelper.badRequestResponse(
            res,
            false,
            "Không có user này trong hệ thống!",
            null
        );
    }

    // Check password with password hash
    const isValidatePassword = bcrypt.compareSync(password, result.password);
    if (!isValidatePassword) {
        return responseHelper.badRequestResponse(
            res,
            false,
            "Mật khẩu không hợp lệ!",
            null
        );
    }

    // Check if account is activated
    if (!result.isActivated) {
        return responseHelper.badRequestResponse(
            res,
            false,
            "Tài khoản chưa được kích hoạt!",
            null
        );
    }

    // Generate jwt
    var token = helper.generateAccessToken({ id: result._id });
    return responseHelper.okResponse(res, true, "", {
        user: User.getUserWithoutPasswordField(result),
        accessToken: token,
        refreshToken: "refreshToken",
    });
};

const register = async (req, res) => {
    const { email, password, displayName } = req.body;

    // Check email vs password is not null

    //Check valid Email
    if (!password || !displayName || helper.checkEmailIsValid(email) == false) {
        return responseHelper.badRequestResponse(
            res,
            false,
            "Display name, email và password bị thiếu hoặc không hợp lệ!",
            null
        );
    }

    // Get a user by email
    const result = await User.getSingleUserByEmail(email);
    if (result) {
        return responseHelper.badRequestResponse(
            res,
            false,
            "Email này đã có người sử dụng!",
            null
        );
    }

    User.addUser(req.body)
        .then((result) => {
            const ActivationAccountToken = helper.generateActivationToken({
                id: result._id,
            });
            var emailContent = `<div>
                <h2>Hi, ${result.displayName}!</h2>
                <p>Thank you for registering in our system</p>
                <p>To activate your account and use our application, please click on the following link to confirm your email.</p>
                <div><a href="http://localhost:${process.env.WEB_PORT}/auth/callback/${ActivationAccountToken}">Activation link</a></div>
                <div><a href="http://${process.env.SERVER_HOST}:${process.env.PORT}/users/activation/${ActivationAccountToken}">SERVER Activation link</a></div>
                <p>Thanks,</p>
                <p>DATN Team.</p>
            </div>`;

            var mailOptions = {
                from: `doantotnghiep@gmail.com`,
                to: email,
                subject: "[DATN] Create new account",
                html: emailContent,
            };

            helper.emailTransporter.sendMail(
                mailOptions,
                function (error, info) {
                    if (!error) {
                        return responseHelper.okResponse(
                            res,
                            true,
                            "Tạo thành công và đã gửi link kích hoạt tài khoản!",
                            null
                        );
                    } else {
                        return responseHelper.serverErrorResponse(
                            res,
                            false,
                            "Không thể gửi email được!",
                            null
                        );
                    }
                }
            );
        })
        .catch((error) => {
            console.log(error);
            return responseHelper.badRequestResponse(
                res,
                false,
                "Không thể tạo được tài khoản mới tại thời điểm này!",
                null
            );
        });
};

async function changePassword(req, res) {
    const user = req.user;
    const { old_password: oldPassword, password } = req.body;

    if (!oldPassword || !password) {
        return responseHelper.badRequestResponse(
            res,
            false,
            "Password mới và cũ không được bỏ trống!",
            null
        );
    }

    if (!bcryptjs.compareSync(oldPassword, user.password)) {
        return responseHelper.badRequestResponse(
            res,
            false,
            "Mật khẩu cũ không hợp lệ!",
            null
        );
    }

    await UserModel.findOneAndUpdate(
        { _id: req.user.id },
        { password: helper.generateHashPassword(password) }
    );

    // todo: handle error express in global -> handle crash as handle db
    return responseHelper.okResponse(
        res,
        true,
        "Đổi mật khẩu thành công!",
        null
    );
}

async function checkExistEmail(req, res) {
    const { email } = req.body;
    if (helper.checkEmailIsValid(email) == false) {
        return responseHelper.badRequestResponse(
            res,
            false,
            "Email không hợp lệ!",
            null
        );
    }
    const result = await UserModel.findOne({ email: req.body.email });

    if (result === null) {
        return responseHelper.badRequestResponse(
            res,
            false,
            "Không có email này trong hệ thống!",
            null
        );
    }

    return responseHelper.okResponse(
        res,
        true,
        "Email có trong hệ thống!",
        null
    );
}

async function requestResetPassword(req, res) {
    const { email } = req.body;

    if (helper.checkEmailIsValid(email) == false) {
        return responseHelper.badRequestResponse(
            res,
            false,
            "Email không hợp lệ!",
            null
        );
    }

    const user = await UserModel.findOne({ email });
    if (!user)
        return responseHelper.badRequestResponse(
            res,
            false,
            "Không có email này trong hệ thống!",
            null
        );

    // Create OTP code and send email
    const code = totp.generate(email);
    console.log("OTP: ", code);
    console.log("VERIFY: ", totp.check(code, email));

    var emailContent = `<div>
                <h2>Hi, ${user.displayName}!</h2>
                <p>You recently requested to reset your password in our system</p>
                <p>Here is your OTP code in order to recover your password, do not share anyone this code.</p>
                <p><strong>${code}</strong></p>
                <p>If you did not request this code, please ignore it, this code is valid for 5 minutes.</p>
                <p>Many thanks, DATN Team.</p>
            </div>`;

    var mailOptions = {
        from: `doantotnghiep@gmail.com`,
        to: email,
        subject: "[DATN] Request reset password",
        html: emailContent,
    };

    helper.emailTransporter.sendMail(mailOptions, async function (error, info) {
        if (!error) {
            return responseHelper.okResponse(
                res,
                true,
                "Đã gửi otp code qua email thành công!",
                null
            );
        } else {
            return responseHelper.serverErrorResponse(
                res,
                false,
                "Hệ thống không thể gửi mail!",
                null
            );
        }
    });
}

async function verifyOtpResetPassword(req, res) {
    const { email, otpCode } = req.body;

    if (helper.checkEmailIsValid(email) == false || !otpCode) {
        return responseHelper.badRequestResponse(
            res,
            false,
            "Email không hợp lệ hoặc body không đủ!",
            null
        );
    }

    const user = await UserModel.findOne({ email });
    if (!user)
        return responseHelper.badRequestResponse(
            res,
            false,
            "Email không tồn tại trong hệ thống!",
            null
        );

    const isValidOTPCode = totp.check(otpCode, email);
    console.log(otpCode, email, isValidOTPCode);
    if (isValidOTPCode) {
        return responseHelper.okResponse(res, true, "OTP hợp lệ!", null);
    } else {
        return responseHelper.badRequestResponse(
            res,
            false,
            "OTP không hợp lệ!",
            null
        );
    }
}

async function activateAccount(req, res) {
    const { activationToken } = req.params;
    const object = await helper.decodeActivationToken(activationToken);
    const user = await User.getSingleUserById(object.id);
    console.log(object);

    if (!user) {
        return responseHelper.badRequestResponse(
            res,
            false,
            "Tài khoản này không tồn tại!",
            null
        );
    }

    if (user.isActivated == true) {
        return responseHelper.okResponse(
            res,
            true,
            "Tài khoản đã được active!",
            user
        );
    }
    user.isActivated = true;
    await user.save();
    const defaultProject = new Project({
        name: "Inbox",
        description: "Inbox project",
        authorId: user.id,
    });
    await defaultProject.save();
    return responseHelper.okResponse(
        res,
        true,
        "Kích hoạt tài khoản thành công!",
        user
    );
}

async function requestActivateAccount(req, res) {
    const { email } = req.body;
    const user = await User.getSingleUserByEmail(email);

    if (!user) {
        return responseHelper.badRequestResponse(
            res,
            true,
            "Tài khoản này không tồn tại!",
            null
        );
    }

    if (user.isActivated == true) {
        return responseHelper.okResponse(
            res,
            false,
            "Tài khoản đã được active!",
            null
        );
    }

    const ActivationAccountToken = helper.generateActivationToken({
        id: user._id,
    });

    var emailContent = `<div>
    <h2>Hi, ${user.displayName}!</h2>
    <p>Thank you for registering in our system</p>
    <p>To activate your account and use our application, please click on one of the following links to confirm your email.</p>
    <div><a href="http://localhost:${process.env.WEB_PORT}/auth/callback/${ActivationAccountToken}">WEBSITE Activation link</a></div>
    <div><a href="http://${process.env.SERVER_HOST}:${process.env.PORT}/users/activation/${ActivationAccountToken}">SERVER Activation link</a></div>
    <p>Thanks,</p>
    <p>DATN Team.</p>
    </div>`;

    var mailOptions = {
        from: `doantotnghiep@gmail.com`,
        to: email,
        subject: "[DATN] Activate new account",
        html: emailContent,
    };

    helper.emailTransporter.sendMail(mailOptions, function (error, info) {
        if (!error) {
            return responseHelper.okResponse(
                res,
                false,
                "Đã gửi email link kích hoạt tài khoản!",
                null
            );
        }
        return responseHelper.serverErrorResponse(
            res,
            false,
            "Không thể gửi email kích hoạt tài khoản được!",
            null
        );
    });
}

async function resetPassword(req, res) {
    const { email, otpCode, newPassword } = req.body;

    if (helper.checkEmailIsValid(email) == false || !otpCode || !newPassword) {
        return responseHelper.badRequestResponse(
            res,
            false,
            "Email không hợp lệ hoặc body không đủ!",
            null
        );
    }

    const user = await UserModel.findOne({ email });
    if (!user)
        return responseHelper.badRequestResponse(
            res,
            false,
            "Email không tồn tại trong hệ thống!",
            null
        );

    const isValidOTPCode = totp.check(otpCode, email);
    if (!isValidOTPCode) {
        return responseHelper.badRequestResponse(
            res,
            false,
            "OTP không hợp lệ!",
            null
        );
    }

    user.password = helper.generateHashPassword(newPassword);
    await user
        .save()
        .then((result) => {
            return responseHelper.okResponse(
                res,
                true,
                "Đã đổi mật khẩu mới thành công!",
                null
            );
        })
        .catch((error) => {
            return responseHelper.serverErrorResponse(
                res,
                false,
                "Có lỗi xảy ra, xin vui lòng thử lại!",
                null
            );
        });
}

async function saveAccountOAuth(req, res) {
    const { avatar, type, displayName, idAccountOwner } = req.body;

    let user = await UserModel.findOne({ idAccountOwner });
    if (user) {
        if (user.displayName !== displayName || user.avatar !== avatar) {
            user.displayName = displayName;
            user.avatar = avatar;
            await user.save();
        }
    } else {
        user = await UserModel.create({
            avatar,
            type,
            displayName,
            idAccountOwner,
        });
    }

    var token = helper.generateAccessToken({ id: user._id });

    return responseHelper.okResponse(res, true, null, {
        user,
        accessToken: token,
        refreshToken: "refreshToken",
    });
}

async function verifyFacebookLogin(req, res) {
    const { user } = req;
    var token = helper.generateAccessToken({ id: user._id });

    return responseHelper.okResponse(res, true, null, {
        jwtToken: token,
        user: user,
    });
}

async function verifyGoogleLogin(req, res) {
    const { user } = req;
    var token = helper.generateAccessToken({ id: user._id });

    return responseHelper.okResponse(res, true, null, {
        jwtToken: token,
        user: user,
    });
}

module.exports = {
    login,
    register,
    changePassword,
    checkExistEmail,
    requestResetPassword,
    verifyOtpResetPassword,
    resetPassword,
    activateAccount,
    requestActivateAccount,
    saveAccountOAuth,
    verifyFacebookLogin,
    verifyGoogleLogin,
};
