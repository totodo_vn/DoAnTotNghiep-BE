const bcryptjs = require("bcryptjs");
const randomstring = require("randomstring");
const { totp } = require("otplib");
const { Task } = require("../models/task.model");
const { Label } = require("../models/label.model");
const { Project } = require("../models/project.model");
const bcrypt = require("bcryptjs");
const nodemailer = require("nodemailer");
const helper = require("../utils/helper");
var mongoose = require('mongoose');

const createNewTask = async (req, res) => {
    const {
        projectId,
        sectionId,
        priority,
        name,
        description,
        point,
        isMember,
        isStarred,
        isImportant,
        isTrashed,
        isCompleted,
        completedDate,
        dueDate,
        labelIds,
        checkList,
        attachmentInfos,
        crontabSchedule,
        preciseSchedules } = req.body;

    // Check if there's a project by projectId
    const existingProject = await Project.find({$or: [{ _id: projectId }]});    
    if (existingProject.length === 0) {
        return res.status(400).json({
            succeeded: false,
            message: "Không thể tìm được project hiện tại",
            result: null
        })
    }  

    // Check task name is valid
    const existingRecords = await Task.find({$or: [{ name: name }]});    
    if (existingRecords.length !== 0) {
        return res.status(400).json({
            succeeded: false,
            message: "Trùng name hoặc color",
            result: null
        })
    }

    // Check if there's a section by sectionId, default is 1 (inbox)
    if (!sectionId) sectionId = 1;
    const existingSectionInProject = await Project.find({$or: [{ "sections._id": sectionId }]});    
    if (existingSectionInProject.length === 0) {
        return res.status(400).json({
            succeeded: false,
            message: "Không thể tìm được section trong project hiện tại",
            result: null
        })
    }

    // Check label ids
    const existingLabels = await Label.find().where('_id').in(labelIds).exec();
    console.log("labels");

    await Task.create({
        projectId,
        sectionId,
        priority,
        name,
        description,
        point,
        isMember,
        isStarred,
        isImportant,
        isTrashed,
        isCompleted,
        completedDate,
        dueDate,
        labelIds,
        checkList,
        attachmentInfos,
        crontabSchedule,
        preciseSchedules
    })
        .then(result => {
            res.json({
                succeeded: true,
                message: "OK",
                result: result,
            });
        })
        .catch(error => {
            res.status(500).json({
                succeeded: false,
                message: "Something's wrong",
                result: null,
        })
    });
}

const getTaskDetail = async (req, res) => {
    const {
        taskId
    } = req.params;

    // Find task record
    const existingTask = await Task.findOne({$or: [{ _id: taskId }]});    
    if (!existingTask) {
        return res.status(400).json({
            succeeded: false,
            message: "Không thể tìm được task hiện tại",
            result: null
        })
    }

    const { projectId, sectionId, labelIds } = existingTask;
    // Check if there's a project by projectId
    const existingProject = await Project.findOne({ _id: projectId });
    if (!existingProject) {
        return res.status(400).json({
            succeeded: false,
            message: "Không thể tìm được project của task này",
            result: null
        })
    }
    
    // Check if there's a section by sectionId, default is 1 (inbox)
    const { sections } = existingProject;
    const existingSectionInProject = sections.find(t => sectionId == t.id);
    if (!existingSectionInProject) {
        return res.status(400).json({
            succeeded: false,
            message: "Không thể tìm được section của task này",
            result: null
        })
    }

    // Check label ids
    const existingLabels = await Label.find().where('_id').in(labelIds).exec();

    let entityToReturn = {...existingTask}._doc;
    entityToReturn = { ...entityToReturn, 
        project: existingProject,
        section: existingSectionInProject,
        labels: existingLabels,
    }

    // First find task detail
    res.json({
        succeeded: true,
        message: "OK",
        result: entityToReturn,
    });
}

const getAllTasks = async (req, res) => {
    const [ allTasks, allProjects, allLabels ] = await Promise.all([
        Task.find(),
        Project.find(),
        Label.find()
    ]) 
        .then(result => result)
        .catch(error => {
            res.status(400).json({
                succeeded: false,
                message: "Hệ thống bị lỗi khi tìm dữ liệu",
                result: null,
            });
        });

    console.log(allTasks);
    const results = allTasks.map((task, index) => {
        const { projectId, sectionId, labelIds } = task;
        // Check if there's a project by projectId
        const existingProject = allProjects.find(t => projectId == t._id);
        if (!existingProject) {
            return res.status(400).json({
                succeeded: false,
                message: "Không thể tìm được project của 1 task trong hệ thống",
                result: null
            })
        }
        
        // Check if there's a section by sectionId, default is 1 (inbox)
        const { sections } = existingProject;
        const existingSectionInProject = sections.find(t => sectionId == t.id);
        if (!existingSectionInProject) {
            return res.status(400).json({
                succeeded: false,
                message: "Không thể tìm được section của task này",
                result: null
            })
        }

        const existingLabels = allLabels.filter((label) => {
            return labelIds.indexOf(label._id) !== -1;
        })
        let entityToReturn = {...task}._doc;
        entityToReturn = { ...entityToReturn, 
            project: existingProject,
            section: existingSectionInProject,
            labels: existingLabels
        }
        return entityToReturn;
    })

    

    // First find task detail
    res.json({
        succeeded: true,
        message: "OK",
        result: results,
    });
}


const updateTaskDetail = async (req, res) => {
    const {
        projectId,
        sectionId,
        priority,
        name,
        description,
        point,
        isMember,
        isStarred,
        isImportant,
        isTrashed,
        isCompleted,
        labelIds,
        checkListInfos,
        attachmentInfos, } = req.body;

    // Check if there's a project by projectId
    const existingProject = await Project.find({$or: [{ _id: projectId }]});    
    if (existingProject.length === 0) {
        return res.status(400).json({
            succeeded: false,
            message: "Không thể tìm được project hiện tại",
            result: null
        })
    }  

    // Check task name is valid
    const existingRecords = await Task.find({$or: [{ name: name }]});    
    if (existingRecords.length !== 0) {
        return res.status(400).json({
            succeeded: false,
            message: "Trùng name hoặc color",
            result: null
        })
    }

    // Check if there's a section by sectionId, default is 1 (inbox)
    if (!sectionId) sectionId = 1;
    const existingSectionInProject = await Project.find({$or: [{ "sections._id": sectionId }]});    
    if (existingSectionInProject.length === 0) {
        return res.status(400).json({
            succeeded: false,
            message: "Không thể tìm được section trong project hiện tại",
            result: null
        })
    }

    // Check label ids
    const existingLabels = await Label.find().where('_id').in(labelIds).exec();

    await Task.create({
        projectId,
        sectionId,
        labelIds,
        priority,
        name,
        description,
        point,
        isMember,
        isStarred,
        isImportant,
        isTrashed,
        isCompleted,
        checkListInfos,
        attachmentInfos
    })
        .then(result => {
            res.json({
                succeeded: true,
                message: "OK",
                result: result,
            });
        })
        .catch(error => {
            res.status(500).json({
                succeeded: false,
                message: "Something's wrong",
                result: null,
        })
    });
}

module.exports = {
    createNewTask,
    getTaskDetail,
    getAllTasks,
    updateTaskDetail
};
