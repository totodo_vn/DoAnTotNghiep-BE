const bcryptjs = require("bcryptjs");
const randomstring = require("randomstring");
const { totp } = require("otplib");
const { Project } = require("../models/project.model");
var mongoose = require('mongoose');
const bcrypt = require("bcryptjs");
const nodemailer = require("nodemailer");
const helper = require("../utils/helper");
const { User } = require("../models/user.model");

const createNewProject = async (req, res) => {
    const {
        name,
        color,
        isFavorite,
        viewType,
        description,
    } = req.body;
    const authorId = req.user._id;

    const existingRecords = await Project.find({$or: [{ name: name, authorId: req.user._id }]});
    if (existingRecords.length !== 0) {
        return res.status(400).json({
            succeeded: false,
            message: "Project tên đã bị trùng",
            result: null
        })
    }

    // Auto add default section Inbox
    const entity = new Project({
        name,
        description,
        color,
        isFavorite,
        viewType,
        authorId,
    });

    await entity.save()
        .then(result => {
            res.json({
                succeeded: true,
                message: "Create OK!",
                result: result,
            })
        })
        .catch(error => {
            res.status(500).json({
                succeeded: false,
                message: error._message || "Something's wrong!",
                result: null
            })
        });
}

const getProjectDetail = async (req, res) => {
    const {
        projectId
    } = req.params;
    const users = await User.find();
    
    console.log(req.user._id);
    // First find project detail
    await Project.findOne({ _id: projectId, authorId: req.user._id })
        .then(result => {
            const project = {...result}._doc;
            const author = users.find(t => result.authorId.equals(t._id));
            project.author = { displayName: author.displayName };
            res.json({
                succeeded: true,
                message: "OK",
                result: project,
            });
        })
        .catch(error => {
            res.status(400).json({
                succeeded: false,
                message: "Something's wrong",
                result: null,
        })
    });
}

const getAllProjects = async (req, res) => {
    // Get all projects

    const [projects, users] = await Promise.all([
        Project.find({ authorId: req.user._id }),
        User.find()
    ])
        .then(result => result)
        .catch(error => {
            res.status(400).json({
                succeeded: false,
                message: "Something's wrong",
                result: null,
            });
        });

    const newProjects = projects.map((project, index) => {
        const newObject = {...project}._doc;
        const author = users.find(t => project.authorId.equals(t._id));
        newObject.author = { 
            displayName: author.displayName 
        };
        return newObject;
    });
    res.json({
        succeeded: true,
        message: "OK",
        result: newProjects,
    });
}

const createNewSectionInProject = async (req, res) => {
    const {
        name,
        description,
    } = req.body;
    const { projectId } = req.params;

    // Find project by projectId
    const project = await Project.findOne({ _id: projectId });
    if (!project) {
        return res.status(400).json({
            succeeded: false,
            message: "Không tìm được project này",
            result: null
        })
    }

    // get all sections in project and check name is valid
    const { sections } = project;
    const existingSectionName = sections.find(t => t.name === name);
    if (existingSectionName) {
        return res.status(400).json({
            succeeded: false,
            message: "Section đã trùng tên",
            result: null
        })
    }

    // create new ID and add to sections
    var id = mongoose.Types.ObjectId();
    sections.push({
        id: id,
        name,
        description,
    });

    await project.save()
        .then(result => res.json({
            succeeded: true,
            message: "Create OK!",
            result: sections,
        }))
        .catch(error => res.status(500).json({
            succeeded: false,
            message: error._message || "Something's wrong!",
            result: null
        })
    );
}


module.exports = {
    createNewProject,
    getProjectDetail,
    getAllProjects,
    createNewSectionInProject,
};
