const bcryptjs = require("bcryptjs");
const randomstring = require("randomstring");
const { totp } = require("otplib");
const { Label } = require("../models/label.model");
const bcrypt = require("bcryptjs");
const nodemailer = require("nodemailer");
const helper = require("../utils/helper");

const createNewLabel = async (req, res) => {
    const {
        name,
        color,
        isFavorite,
    } = req.body;

    const existingRecords = await Label.find({$or: [{ name: name }]});

    if (existingRecords.length !== 0) {
        return res.status(400).json({
            succeeded: false,
            message: "Trùng name hoặc color",
            result: null
        })
    }

    await Label.create({
        name,
        color,
        isFavorite,
    })
        .then(result => {
            res.json({
                succeeded: true,
                message: "Create OK!",
                result: result,
            })
        })
        .catch(error => {
            console.log(error);
            res.status(500).json({
                succeeded: false,
                message: error._message || "Something's wrong!",
                result: null
            })
        });
}

const getLabelDetail = async (req, res) => {
    const {
        labelId
    } = req.params;

    await Label.findOne({ _id: labelId })
        .then(result => {
            res.json({
                succeeded: true,
                message: "OK",
                result: result,
            });
        })
        .catch(error => {
            res.status(500).json({
                succeeded: false,
                message: "Something's wrong",
                result: null,
        })
    });
}

const updateLabelDetail = async (req, res) => {
    const { labelId } = req.params;
    const { name, color, isFavorite } = req.body;

    const existingRecords = await Label.find({$or: [{ name: name }, { color: color }]});    
    if (existingRecords.length !== 0) {
        return res.status(400).json({
            succeeded: false,
            message: "Trùng name hoặc color",
            result: null
        })
    }
    
    await Label.updateOne({ _id: labelId }, req.body)
        .then(result => {
            res.json({
                succeeded: true,
                message: "OK",
                result: null,
            });
        })
        .catch(error => {
            res.status(500).json({
                succeeded: false,
                message: "Something's wrong",
                result: null,
        })
    });
}

const toggleIsFavoriteLabel = async (req, res) => {
    const { labelId } = req.params;

    const label = await Label.findOne({ _id: labelId });
    label.isFavorite = !label.isFavorite;
    await label.save()
        .then(result => {
            res.json({
                succeeded: true,
                message: "OK",
                result: null,
            });
        })
        .catch(error => {
            res.status(500).json({
                succeeded: false,
                message: "Something's wrong",
                result: null,
        })
    });
}


const getAllLabels = async (req, res) => {
    await Label.find()
        .then(result => {
            res.json({
                succeeded: true,
                message: "OK",
                result: result,
            });
        })
        .catch(error => {
            res.status(500).json({
                succeeded: false,
                message: "Something's wrong",
                result: null,
        })
    });
}

module.exports = {
    createNewLabel,
    getLabelDetail,
    getAllLabels,
    updateLabelDetail,
    toggleIsFavoriteLabel
};
