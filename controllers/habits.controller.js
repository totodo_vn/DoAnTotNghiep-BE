const { habitModelControl, HabitModel } = require("../models/habit.model");

const createHabit = async (req, res) => {
    // todo: validate req body client sent

    const result = await habitModelControl.addHabit(req.body);

    res.status(201).json({
        succeeded: true,
        message: "OK",
        result,
    });
};

const getAll = async (req, res) => {
    const result = await habitModelControl.getHabits();
    return res
        .status(200)
        .json({ succeeded: true, message: "OK", result: result });
};

const getById = async (req, res) => {
    const { id } = req.params;

    try {
        let result = await habitModelControl.getById(id);
        return res.status(200).json({ succeeded: true, message: "OK", result });
    } catch (error) {
        return res.status(400).json({
            succeeded: false,
            message: "Id is not exist",
            result: null,
        });
    }
};

const deleteHabit = async (req, res) => {
    const { id } = req.params;
    try {
        let result = await habitModelControl.deletedById(id);
        console.log(result);
        return res.status(200).json({ succeeded: true, message: "OK", result });
    } catch (error) {
        console.log(error);
        return res.status(400).json({
            succeeded: false,
            message: "Id is not exist",
            result: null,
        });
    }
};

const checkIn = async (req, res) => {
    const { id } = req.params;
    const { diaries, date } = req.body;

    let checkInInfo = {
        date,
    };
    const resultQuery = await HabitModel.findOne({
        _id: id,
        "habitProgress.date": date,
    });

    if (resultQuery === null) {
        await HabitModel.findOneAndUpdate(
            { _id: id },
            { $push: { habitProgress: checkInInfo } },
            { new: true }
        );
    }
    await HabitModel.findOneAndUpdate(
        { _id: id, "habitProgress.date": date },
        { $inc: { "habitProgress.$.currentCheckInTimes": 1 } },
        { new: true }
    );

    await updateHabitInDateIsDone(id, date);

    res.status(200).json({
        succeeded: false,
        message: "OK",
        result: null,
    });
};

const updateHabitInDateIsDone = async (id, date) => {
    const resultQuery = await HabitModel.findOne({
        _id: id,
        "habitProgress.date": date,
    });

    if (resultQuery !== null) {
        console.log("here");
        let totalTimesMustCheckIn = resultQuery.totalTimesMustCheckIn;
        const result = await HabitModel.findOneAndUpdate(
            {
                _id: id,
                "habitProgress.date": date,
                "habitProgress.currentCheckInTimes": {
                    $gte: totalTimesMustCheckIn,
                },
            },
            { $set: { "habitProgress.$.isDone": true } },
            { new: true }
        );
        console.log(result);
    }
};

const finishHabit = async (req, res) => {
    const { id } = req.params;
    try {
        let result = await HabitModel.findOneAndUpdate(
            {
                _id: id,
            },
            { isFinished: true },
            { new: true }
        );
        return res.status(200).json({ succeeded: true, message: "OK", result });
    } catch (error) {
        return res.status(400).json({
            succeeded: false,
            message: "Id is not exist",
            result: null,
        });
    }
};

module.exports = {
    createHabit,
    getAll,
    getById,
    deleteHabit,
    checkIn,
    finishHabit,
};
