const userTypes = require("./types-user");
const viewTypes = require("./types-view");

module.exports = {
    userTypes,
    viewTypes
};
