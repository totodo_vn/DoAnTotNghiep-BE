const mongoose = require("mongoose");
const passport = require("passport");
const LocalStrategy = require("passport-local");
const FacebookTokenStrategy = require("passport-facebook-token");
const GoogleTokenStrategy = require("passport-google-token").Strategy;
const { User: UserModel } = require("../models/user.model");
const { userTypes } = require("../constants");

passport.use(
    new FacebookTokenStrategy(
        {
            clientID: process.env.FACEBOOK_APP_ID,
            clientSecret: process.env.FACEBOOK_APP_SECRET,
        },
        (accessToken, refreshToken, profile, done) => {
            // console.log(accessToken);
            // console.log(profile);
            UserModel.findOrCreate(
                { idAccountOwner: profile.id },
                {
                    avatar: profile.photos[0].value,
                    type: userTypes.FACEBOOK,
                    displayName: profile.displayName,
                    email: profile.emails[0].value,
                    isActivated: true,
                },
                (error, user) => done(error, user)
            );
        }
    )
);

passport.use(
    new GoogleTokenStrategy(
        {
            clientID: process.env.GOOGLE_CLIENT_ID,
            clientSecret: process.env.GOOGLE_CLIENT_SECRET,
            callbackURL: "/auth/google/redirect"
        },
        (accessToken, refreshToken, profile, done) => {
            UserModel.findOrCreate(
                { idAccountOwner: profile.id },
                {
                    avatar: profile._json.picture,
                    type: userTypes.GOOGLE,
                    displayName: profile.displayName,
                    email: profile.emails[0].value,
                    isActivated: true,
                },
                (err, user) => done(err, user)
            );
        }
    )
);

passport.serializeUser((user, done) => {
    done(null, user);
});

passport.deserializeUser((user, done) => {
    done(null, user);
});
