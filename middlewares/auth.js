const { response } = require("express");
const jwt = require("jsonwebtoken");
const { User } = require("../models/user.model");
const responseHelper = require("../utils/response");

module.exports = function (req, res, next) {
    const authHeader = req.headers["authorization"];
    const token = authHeader && authHeader.split(" ")[1];

    if (token == null) {
        return responseHelper.unauthorizedResponse(
            res,
            false,
            "Chưa đăng nhâp",
            null
        );
    }

    jwt.verify(token, process.env.JWT_SECRET_KEY, async (err, result) => {
        if (err || !result)
            return responseHelper.unauthorizedResponse(res, false, "", null);
        const user = await User.findById(result.id);
        if (!user) {
            responseHelper.notFoundResponse(
                res,
                false,
                "Tài khoản không tồn tại",
                null
            );
        }
        req.user = user;
        next();
    });
};
