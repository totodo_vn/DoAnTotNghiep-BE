const mongoose = require('mongoose');

mongoose.Promise = global.Promise;
// process.env.mongoURL ||
const connectDB = async () => { 
  await mongoose
    .connect(process.env.DB_CONNECT_URL, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
    })
    .then(() => {
      console.log("Successfully connected to the database");
    })
    .catch((err) => {
      console.log("Could not connected to the database. Exiting now...", err);
      process.exit();
    });
}


module.exports = connectDB;
