exports.okResponse = (res, succeeded, message, result) => {
    return res.json({ succeeded, message, result });
};

exports.badRequestResponse = (res, succeeded, message, result) => {
    return res.status(400).json({ succeeded, message, result });
};

exports.unauthorizedResponse = (res, succeeded, message, result) => {
    return res.status(401).json({ succeeded, message, result });
};

exports.forbiddenResponse = (res, succeeded, message, result) => {
    return res.status(403).json({ succeeded, message, result });
};

exports.notFoundResponse = (res, succeeded, message, result) => {
    return res.status(404).json({ succeeded, message, result });
};

exports.serverErrorResponse = (res, succeeded, message, result) => {
    return res.status(500).json({ succeeded, message, result });
};
