const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const nodemailer = require("nodemailer");
const { User } = require("../models/user.model");

const generateHashPassword = (passwordString) => {
    const salt = 10;
    return bcrypt.hashSync(passwordString, salt);
};

const generateAccessToken = (info) => {
    return jwt.sign({ ...info }, process.env.JWT_SECRET_KEY, {
        expiresIn: 3600,
    });
};

const generateActivationToken = (info) => {
    return jwt.sign({ ...info }, process.env.JWT_ACTIVATION_KEY, {
        expiresIn: 86400,
    });
};

const decodeActivationToken = async (token) => {
    try {
        return await jwt.verify(token, process.env.JWT_ACTIVATION_KEY);
    } catch {
        return null;
    }
};

const emailTransporter = nodemailer.createTransport({
    host: "smtp.gmail.com",
    port: 465,
    secure: true,
    auth: {
        user: "khactrieuhcmus@gmail.com",
        pass: "khactrieuserver",
    },
    tls: {
        rejectUnauthorized: false,
    },
});

// Return true if email is valid type
const checkEmailIsValid = (email) => {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
};

module.exports = {
    generateAccessToken,
    generateActivationToken,
    generateHashPassword,
    emailTransporter,
    checkEmailIsValid,
    decodeActivationToken,
};
